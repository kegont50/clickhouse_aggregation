import json

import uvicorn
from fastapi import FastAPI
from starlette.responses import PlainTextResponse

from db import client

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/getWords", response_class=PlainTextResponse)
async def count_occurrence():
    query = """
            select
                words,
                count(*) word_count
            from
            (
                    SELECT arrayJoin(arrayFilter(x -> x!= '', splitByString(' ', 
                    (replaceRegexpAll(lowerUTF8(assumeNotNull(text)), '[^\\p{L}\\p{N} ]', ''))))) as words
                    from lenta.`lenta-ru-news`
            )
            GROUP BY words
            ORDER BY word_count DESC
"""
    words = client.execute(query)[:100]

    result_dict = {item[0]: item[1] for item in words}
    return json.dumps({"words_and_counts": result_dict}, ensure_ascii=False)

if __name__ == "__main__":
    uvicorn.run(app, port=9090)
